#!/bin/bash 
# Run the tests of the RDF python script.
# The script can be run without arguments to run all the tests,
# or with an integer to run a particular test.

# Conrad Johnston - Nov 2017

run=('false' 'false' 'false')

if [ "$#" -eq 1 ] ; then 
    run[$(($1-1))]='true'  
elif [ "$#" -eq 0 ] ; then 
    run=('true' 'true' 'true')
else
    echo "ERROR: Too many arguments"
    exit 1
fi

if [ "${run[0]}" == "true" ] ; then

    #1) Can a simple cubic RDF be reproduced? 
    #Uses: test_h2o_20ang.xyz
    #Checked against: VMD
    
    echo "-------------------------------------------------------------------------"
    echo "Test 1: Water - Cubic Box - Single Frame"
    echo "-------------------------------------------------------------------------"
    printf "Running...\n\n"
    
    python RDF_NonOrtho.py XYZs/test_h2o_20ang.xyz 10.0 0.1 100 O O 20.,0.,0.,0.,20.,0.,0.,0.,20.
    rmse=$(python CalcRMSE.py histogram.dat Refs/rdf_h2o_allO_ref.dat)
    printf "\nHistogram RMSE = %s\n" $rmse
    
    printf "\nPlotting..."
    gnuplot -e "set xlabel 'R (angs)'; set ylabel 'g(r)'; \
                plot 'histogram.dat' w p pt 7 lc rgb 'red' lw 2 title 'Histogram',\
                'kde.dat' u 1:2 w l dt 3 lc rgb 'blue' lw 2 title 'KDE',\
                'Refs/rdf_h2o_allO_ref.dat' u 1:2 w l lc rgb 'black' lw 2 title 'Reference Histogram'" --persist
    printf "\rPlotting...DONE\n\n"

fi
    
if [ "${run[1]}" == 'true' ] ; then

    #2) Can a non orthogonal cell be reproduced?
    #Uses: test_cubic_ortho.xyz
    #test_cubic_parallelepiped.xyz
    #Checked against: the orthocase (the atomic lattice is cubic in both cases)
    
    echo "-------------------------------------------------------------------------"
    echo "Test 2: Cubic Crystal - Orthorhombic vs Parallelepiped - Single Frame"
    echo "-------------------------------------------------------------------------"
    printf "Running the orthorhombic case...\n\n"
    
    python RDF_NonOrtho.py XYZs/test_cubic_ortho.xyz 5.0 0.1 50 C C 10.,0.,0.,0.,10.,0.,0.,0.,10.
    mv histogram.dat histogram.dat.ortho
    mv kde.dat kde.dat.ortho
    
    printf "\n\nRunning the parallelepiped case...\n\n"
    
    python RDF_NonOrtho.py XYZs/test_cubic_parallelepiped.xyz 5.0 0.1 50 C C 10.,0.,0.,10.,10.,0.,0.,0.,10.
    mv histogram.dat histogram.dat.para
    mv kde.dat kde.dat.para
    
    rmse=$(python CalcRMSE.py histogram.dat.ortho histogram.dat.para)
    printf "\nHistogram RMSE = %s\n" $rmse
    rmse=$(python CalcRMSE.py kde.dat.ortho kde.dat.para)
    printf "\nKDE RMSE = %s\n" $rmse
    
    printf "\nPlotting..."
    gnuplot -e "set xlabel 'R (angs)'; set ylabel 'g(r)'; \
                plot 'histogram.dat.ortho' u 1:2 w l lc rgb 'black' lw 2 title 'Histogram: Orthographic',\
                'histogram.dat.para' w lp pt 7 dt 3 lc rgb 'red' lw 2 title 'Histogram: Parallelepiped',\
                'kde.dat.ortho' u 1:2 w l lc rgb 'blue' lw 2 title 'KDE: Orthographic',\
                'kde.dat.para' u 1:2 w l dt 3 lc rgb 'blue' lw 2 title 'KDE: Parallelepiped'" --persist
    printf "\rPlotting...DONE\n\n"

    rm -f kde.dat.para kde.dat.ortho histogram.dat.para histogram.dat.ortho

fi
    
if [ "${run[2]}" == "true" ] ; then

    #3) Are trajectories correctly averaged?
    #Uses: test_h2o_traj.xyz
    #Checked against: VMD
    
    echo "-------------------------------------------------------------------------"
    echo "Test 3: Water - Averaging - Trajectory"
    echo "-------------------------------------------------------------------------"
    printf "Running...\n\n"
    
    python RDF_NonOrtho.py XYZs/test_h2o_traj_short.xyz 10.0 0.1 100 O O 23.70392558748520,0.,0.,0.,23.70392558748520,0.,0.,0.,23.70392558748520
    
    rmse=$(python CalcRMSE.py histogram.dat Refs/rdf_h2o_allO_traj_short_ref.dat)
    printf "\nHistogram RMSE = %s\n" $rmse
    
    printf "\nPlotting..."
    gnuplot -e "set xlabel 'R (angs)'; set ylabel 'g(r)'; \
                plot 'Refs/rdf_h2o_allO_traj_short_ref.dat' u 1:2 w l lc rgb 'black' lw 2 title 'Reference Histogram',\
                'histogram.dat' u 1:2 w lp dt 3 pt 7 lc rgb 'red' lw 2 title 'Histogram',\
                'kde.dat' u 1:2 w l lc rgb 'blue' lw 2 title 'KDE'" --persist
    printf "\rPlotting...DONE\n\n"

    rm -f histogram.dat kde.dat
fi 
