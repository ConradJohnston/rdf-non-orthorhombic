#!/bin/env python
"""
Short utility to calculate the RMS error between two data sets
The big assumption is that the two data sets are aligned with respect to the x-coordinate,
and that the data points have the same spacing.

Conrad Johnston - Nov 2017
"""

import sys

# Get the two files
file1 = sys.argv[1]
file2 = sys.argv[2]

# Parse y(x) data
data1 = []
for line in open(file1, 'r') :
    if line[0] == '#':
        continue
    try:
        data1.append(line.rstrip('\n').split()[1])
    except:
        continue

data2 = []
for line in open(file2, 'r') :
    if line[0] == '#':
        continue
    try:
        data2.append(line.rstrip('\n').split()[1])
    except:
        continue

# Calculate the summed square error
summed_sq_error = 0.0
for d1,d2 in zip(data1,data2):
    sq_error = (float(d1)-float(d2))**2.0
    summed_sq_error = summed_sq_error + sq_error

# Normalise
n1 = len(data1)
n2 = len(data2)
if n1 < n2 :
    N = n1
else:
    N = n2
rmse = (summed_sq_error/N)**0.5

print('{:.3e}'.format(rmse))



    
