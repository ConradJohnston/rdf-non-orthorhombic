#!/usr/bin/env python

"""

 Monolithic (and slow!) script to calculate the Radial Distribution Function, g(r).
 Respects periodic boundary conditions in all cells, including triclinc cells.

 In this version, both the histogram and kernel density estimation are used.

 Usage: ./RDF_NonOrtho.py [XYZ File] [R Max] [Bin Width] [KDE Grid Points] [First Atom] [Second Atom] [Cell Lattice Matrix]

 Arguments
 -----------
    xyzfilename     - An xyz file to average over
    R Max           - The max range of the RDF (typically must be no more than L/2)
    Bin Width       - The spacial resolution of the RDF
    KDE Grid Points - The spacial resolution of the RDF
    First Atom      - The first atom in the pair distribution function pair. Can be 
                      a string containing an atom label, an index, or 'all' to consider
                      all atoms.
    Second Atom     - As above, the second atom in the pair distribution function pair. 
                      Can be the same as the first.
    Cell Lattice    - A cell lattice matrix of the form:
                          a1 a2 a3
                          b1 b2 b3
                          c1 c2 c3
                      expressed in one comma separarated line:
                          a1,a2,a3,b1,b2,b3,c1,c2,c3
 Returns
 -----------
 A two column RDF dataset to standard output.
 Progess is outputted to standard error.

 Notes
 -----------
 The units of length are in the units of the xyz file.


**Copyright (c) 2017 Conrad Johnston - All rights reserved.**
**Use at your own risk! No warranty provided.**
**Contact: conrad DOT s DOT johnston AT googlemail DOT com**
 -----------

"""

import argparse
import pdb
import sys
import numpy as np
import os.path

def main():   
    
    # Create an object to hold all the input data for convenience
    data = InputData()
    # Set up a 1D array to hold histogram data
    data.histogram,data.nbins = SetupHistogram(data)
    # Set up the grids for KDE
    data.rgrid, data.grgrid, data.deltar = SetupKDEGrid(data)

    # Set up two arrays to hold the indicies of the atoms over which the RDF will
    # be calculated.
    data.atom_refs1 = SetupAtomLists(data.firstref, data.elements)
    data.atom_refs2 = SetupAtomLists(data.secondref, data.elements)
    # Calculate the RDF
    data.histogram, data.kdegrid = CalculateRDF(data)
    # Pretty print the data
    PrintData(data)
    
    return
    

class InputData:

    def __init__(self):
        
        self.args = self.ParseArgs()
        
        # Sanity checks on numerical argumnets
        try:
            self.rmax = float(self.args.r_max)
        except ValueError:
            print("ERROR: R_Max must be a positive, non zero number.")
        try:
            self.bin_w = float(self.args.bin_width)
        except ValueError:
            print("ERROR: Bin width must be a positive, non zero number.")

        if self.args.grid_points.isdigit():
            self.ngrid = int(self.args.grid_points)
        else:
            raise ValueError("ERROR: Grid points must be a positive, non zero integer.")

        if self.rmax <= 0.0 :
            ValueError("ERROR: R_Max must be a positive, non zero number.")
        elif self.bin_w <= 0.0 :
            ValueError("ERROR: Bin width must be a positive, non zero number.")
        elif self.bin_w > self.rmax :
            ValueError("ERROR: Bin width must be less than R_Max.")
        elif self.bin_w == self.rmax :
            ValueError("ERROR: With this bin width, only one bin will be generated!")

        # Try to parse the reference atoms as indicies, otherwise, assume they are element labels
        try:
            self.firstref = int(self.args.first_atom)
        except: 
            self.firstref = self.args.first_atom
        try:
            self.secondref = int(self.args.second_atom)
        except: 
            self.secondref = self.args.second_atom
        
        # Build the cell matrix
        lats=self.args.LatticeParameters.split(',')
        cellmat=np.array(
            [[lats[0],lats[1],lats[2]],
             [lats[3],lats[4],lats[5]],
             [lats[6],lats[7],lats[8]]])
        self.cellmat=cellmat.astype(np.float)

        # Parse the XYZ data
        self.pos_mat,self.elements = self.ParseXYZ()

    def ParseArgs(self): 
        """
        Parse the command line arguments
        """
        
        parser = argparse.ArgumentParser(description=__doc__, usage=__doc__,
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument("xyzfilename",
                help="An xyz file to read"
                )
        parser.add_argument("r_max",
                help="The largest distance to consider"
                )
        parser.add_argument("bin_width",
                help="The width of the bins"
                )
        parser.add_argument("grid_points",
                help="The number of grid points to calculate the KDE on"
                )
        parser.add_argument("first_atom",
                help="The first atom in the pair distribution function pair"
                )
        parser.add_argument("second_atom",
                help="The first atom in the pair distribution function pair"
                )
        parser.add_argument("LatticeParameters",
                help="A list of the lattice parameter vectors, comma separated. i.e. ax,ay,az,bx,by,bz,...",
                )
        return parser.parse_args(namespace=self)
    
    def ParseXYZ(self):
        """
        Read the XYZ file.
        
        Arguments
        ---------
        xyzfile - An xyz file. May be a trajectory.
        
        Returns
        ---------
        pos_mat  - A 3-index numpy array of form [iter,atom_index, xyzdata]
        elements - A 1-index numpy array of atom element labels
        """
        # Parse the XYZ file
        all_lines = []
        for line in open(self.args.xyzfilename):
           all_lines.append(line.rstrip('\n'))
        
        # First line of an xyz file should be an int giving the number of atoms,
        # and should be a multiple of the number of lines, less 2.
        dataline = all_lines[0].split()
        ## Make sure we have an int
        try:
            natom = int(dataline[0])
        except: 
            raise ValueError("Expected an integer on the first line of the xyz file")
        ## Make sure we have all the atoms
        try: 
            len(all_lines)%(natom+2) == 0 
        except:
            raise ValueError("XYZ file hasn't got enough atoms!")
        
        niter = len(all_lines)/(natom+2)
        
        # Allocate some arrays to hold the data. First index is iteration. Second is atom. Third is xyz 
        pos_mat = np.zeros((niter,natom,3))
        # And another to record the order of the elements. These shouldn't change so we only use a 2 matrix and 
        # only parse this data for the first iteration.
        elements = np.empty((natom),dtype='str')
        
        # Get the elements
        for i_atom in range(natom):
            dataline = all_lines[2+i_atom].split()
            elements[i_atom] = dataline[0]
        
        # Run over all lines, pulling out the xyz data
        for ii_iter in range(niter):
            for i_atom in range(natom):
                dataline = all_lines[ii_iter*(natom+2)+2+i_atom].split()
                pos_mat[ii_iter,i_atom,0] = (float(dataline[1]))
                pos_mat[ii_iter,i_atom,1] = (float(dataline[2]))
                pos_mat[ii_iter,i_atom,2] = (float(dataline[3]))
        
        #Save some values for later
        self.natom = natom
        self.niter = niter

        return pos_mat,elements
    
def SetupHistogram(data):
    """
    Generate the histrogram array
    """

    nbins=int(data.rmax/data.bin_w)
    histogram=np.zeros(int(nbins))

    return histogram,nbins

def SetupKDEGrid(data):
    """
    Generate a grid to build the KDE on.
    """

    rgrid = np.linspace(0.0, data.rmax, num=data.ngrid) 
    deltar = (rgrid[-1]-rgrid[0])/data.ngrid
    grgrid = np.zeros(data.ngrid)

    return rgrid,grgrid,deltar

def SetupAtomLists(atom_ref, elements):
    """
    Generate lists of atoms to calculate the RDF over
    """
    if atom_ref == 'all':
        return np.arange(len(elements))
    elif type(atom_ref) is int:
        atom_list = []
        atom_list.append(atom_ref)
        return np.array([atom_ref])
    else:
        atom_list = []
        for ii in range(len(elements)):
            if atom_ref == elements[ii]:
                atom_list.append(ii) 
        return np.asarray(atom_list)

def CalculateRDF(data):
    """
    Calculates the RDF between all pairs of atoms defined in the arrays 
    atom_refs1 and atom_refs2, and averaging over all iterations.
    """

    cellmat = data.cellmat
    cellmat_inv = np.linalg.inv(cellmat)

    nbins = data.nbins
    bins = data.histogram
    bin_w = data.bin_w
    kdegrid = data.grgrid
 
    refs1 = data.atom_refs1
    refs2 = data.atom_refs2
    natom_i = len(refs1)
    natom_j = len(refs2)
    
    UpdateTimer(0,data.niter)

    for ii_iter in range(data.niter):    
        for ii,atom_i in np.ndenumerate(refs1): 
            for jj,atom_j in np.ndenumerate(refs2):
    
                # Don't calculate the the distance from an atom to itself:
                if atom_i == atom_j :
                    continue
                
                # Create two postion vectors
                ri=data.pos_mat[ii_iter,atom_i,:]
                rj=data.pos_mat[ii_iter,atom_j,:]
                # Calculate the absolute distance between them, across PBC
                dist = CalcPairDist(cellmat,cellmat_inv,ri,rj)

                # Ignore those pairs beyond rmax
                if dist >= data.rmax :
                    continue
                else:
                   # Bin the resutls to build the histogram
                   bin_id=int(np.floor(dist/bin_w))
                   bins[bin_id]=bins[bin_id]+1

                   # Calculate the Kernel function, centered on distance
                   bandwidth = 0.3
                   scaledkernel = CalculateKernel(dist,data.rgrid,bandwidth)
                   kdegrid = kdegrid + scaledkernel
            
            UpdateTimer((ii_iter*refs1.shape[0]+ii[0]+1), (data.niter*refs1.shape[0]))
    
    # Cell properties
    volume = abs(np.linalg.det(data.cellmat))
    rho = refs1.shape[0]/volume
    
    ## Calculate g(r) for histogram
    # g(r)=n(r)/(rho * 4pi * r^2 * delta_r * Natoms )
   
    # Average over atoms, iterations and the normalise to total density
    bins = bins/(rho*natom_i*data.niter)
    # Calculate the g(r) using each shell volume
    for ii_bin in range(nbins):
        shell_vol = (4.0/3.0)*np.pi*(((ii_bin+1)*bin_w)**3-(ii_bin*bin_w)**3)
        bins[ii_bin] = bins[ii_bin]/(shell_vol)
   
    ## Calculate g(r) for KDE
    # Normalise KDE
    # Note : Currently there is no correction for edge effects in this KDE. As such,
    # at large r, g(r) tends to drop. The KDE is spilling past the max_r cutoff, and so 
    # some 'area' weight is lost. Similarly, this should happen at r=0.0. A numerical 
    # problem is also present: At small r, the shell volume is very small, and close to 
    # zero. At small r, the uncorrected KDE is also very small, and close to zero.
    # This results in a large number. Currently, the fudge is to set the g(r) to 0.0
    # for first 3 or so bins. Perhaps I could compare the magnitude of the shell_vol
    # and KDE and if they are similar and very small, they could be neglected.

    # Average over atoms, iterations and the normalise to total density and bandwidth
    kdegrid = kdegrid*data.deltar/(bandwidth*rho*data.niter*natom_i)
    
    for ri in range(len(data.rgrid)):
        if ri == 0 :
            shell_vol = 0.0
        else:
            shell_vol = (4.0/3.0)*np.pi*((data.rgrid[ri]**3.0)-(data.rgrid[ri-1]**3.0))
            kdegrid[ri] = kdegrid[ri]/shell_vol

    return bins,kdegrid


def CalcPairDist(cellmat,cellmat_inv,ri,rj):
    """"
    Calculate the distance between two atoms accross periodic boundary conditions.
    Uses the general algorithm for the minimum image (Appendix B - Eq 9.) from:
    M. E. Tuckerman. Statistical Mechanics: Theory and Molecular Simulation. 
    Oxford University Press, Oxford, UK, 2010.

    Arguments
    ----------
        cellmat_inv - The inverse of the 3x3 matrix describing the simulation cell
        ri,rj       - numpy vectors describing the position of atoms i and j

    Returns
    ---------
        dist - The distance between the atoms i and j, according the minimum image 
               convention.
    """
    si=np.dot(cellmat_inv,ri)
    sj=np.dot(cellmat_inv,rj)
    sij=si-sj
    sij=sij-np.rint(sij)
    rij=np.dot(cellmat,sij)
    
    # Get the magnitude of the vector
    dist=np.sqrt(np.dot(rij,rij))
    
    return dist

def CalculateKernel(mu,rgrid,bandwidth):
    """
    Calculate the gaussian kernel for all grid points, centered on mu
    """
    # Grid points to evaluate mew on:
    rscaled = (rgrid - mu) / bandwidth
    scaledkernel = (1/np.sqrt(2*np.pi))*np.exp(-0.5*(rscaled**2))

    return scaledkernel


def UpdateTimer(current_iter,total_iter):
    """
    Print a 'timer' to the terminal to indicate progress.  
    """

    if current_iter == total_iter:
        sys.stderr.write("\rCalculating... 100.0 %")
        sys.stderr.write("\nCompleted!\n")
    else:
        sys.stderr.write(("\rCalculating... {:4.1f} %").format((float(current_iter)/total_iter)*100))
    sys.stderr.flush()

    return 


def PrintData(data):
    """
    Print the data to two files - histogram.dat, and kde.dat
    Maybe add a prefix to the commandline options.
    """

    with open('histogram.dat','w') as f1:
        f1.write('# r         value\n')
        for bin_i in range(data.nbins):
            lne = str("{:9.5f} {:9.5f}\n").format((bin_i*data.bin_w+(data.bin_w/2)),data.histogram[bin_i])
            f1.write(lne)

    with open('kde.dat','w') as f2:
        f2.write('# r         value\n')
        
        #    data.rgrid = data.rgrid[0:len(data.rgrid)/2]
        #    data.kdegrid = data.kdegrid[0:len(data.kdegrid)/2]
        for rpos,value in zip(data.rgrid,data.kdegrid):
            lne = str("{:9.5f} {:9.5f}\n").format(rpos,value)
            f2.write(lne)

    return

if __name__ == "__main__":
    main()
