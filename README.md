# RDF - Non-Orthorhombic #

This a simple Python script to calculate the pair radial distribution function 
from xyz trajectories, for any shape of cell. 

In the current implementation, the RDF is estimated in two ways. Firstly, using 
a histogram, as for traditional implementations. Secondly, with kernel density 
estimation, using a normal guassian as the kernel function.

There are undoubtedly other packages available on the internet already which do
this, and likely much better. This grew form a smaller project so I've made 
available in case it may be use or interest to someone.

* v1.0
* To do: 
    * Integral of g(r)  
    * Boundary effect correction to the kernel density estimate
    * Performance improvements
    * Non spherical distribution functions

### Setup and Usage ###

The script requires Python 2.7+ to be installed, and the test script requires Gnuplot.
The script does not need installed and in principle should work immediately. 
If you want to verify that it is working correctly on your machine, there is an automated set of 
tests that can be run with:  
    `./RunTests.sh`

Otherwise, when happy, use the script with:  
    `./RDF_NonOrtho.py [XYZ File] [R Max] [Bin Width] [KDE Grid Points] [First Atom] [Second Atom] [Cell Lattice Matrix]`

    Arguments
    -----------
    xyzfilename     - An xyz file to average over
    R Max           - The max range of the RDF (typically must be no more than L/2)
    Bin Width       - The width of the bins for the histogram estimate
    KDE Grid Points - The number of grid points for the kernel density estimate
    First Atom      - The first atom in the pair distribution function pair. Can be 
                      a string containing an atom label, an index, or 'all' to consider
                      all atoms.
    Second Atom     - As above, the second atom in the pair distribution function pair. 
                      Can be the same as the first.
    Cell Lattice    - A cell lattice matrix of the form:
                          a1 a2 a3
                          b1 b2 b3
                          c1 c2 c3
                      expressed in one comma separarated line:
                          a1,a2,a3,b1,b2,b3,c1,c2,c3

### About the Tests ###

There are a number of test xyz file and output RDFs in this repository.

These are organised into two directories as follows:

* XYZs/ : These are the sample xyz files.
* Refs/ : These are the corresponding RDF data files produced by the VMD histogram implementation.

The testing strategy is based on the assumption that the VMD implementation works, and so
it is treated as being correct in all cases. However, VMD cannot treat non-orthorhombic cells
and so to test these, special xyz files were produced where the underlying lattice is the same
but with two different primitive unit cells.

The testing is as follows:

   1. Can a simple cubic RDF be reproduced?  
   2. Can a non-orthogonal cell be reproduced?
   3. Are trajectories correctly averaged?

These tests are run automatically by running:
   `./RunTests.sh`

**Copyright (c) 2017 Conrad Johnston - All rights reserved.**  
**Use at your own risk! No warranty provided.**  
**Contact: conrad DOT s DOT johnston AT googlemail DOT com**
